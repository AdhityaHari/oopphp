<?php

// require('animal.php');
require("frog.php");
require("ape.php");

$object = new Animal('shaun');

echo "Name : $object->name <br>";
echo "Legs : $object->legs <br>";
echo "Cold blooded : $object->cold_blooded <br><br>";

$object2 = new Frog('buduk');

echo "Name : $object2->name <br>";
echo "Legs : $object2->legs <br>";
echo "Cold blooded : $object2->cold_blooded <br>";
$object2->jump();
echo "<br><br>";

$object3 = new Ape('kera sakti');

echo "Name : $object3->name <br>";
echo "Legs : $object3->legs <br>";
echo "Cold blooded : $object3->cold_blooded <br>";
$object3->yell();